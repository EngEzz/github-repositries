//
//  RepositryMock.swift
//  roboustaTaskTests
//
//  Created by ahmed ezz on 10/5/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class MockRepositry: DataRepositry {
    private var mockConnection:MockConnection
    init(mockConnection:MockConnection) {
        self.mockConnection = mockConnection
    }
    override func getUserRepos(_ successCompletion: (([RepositryModel]?) -> Void)?, _ failureCompletion: ((Error?) -> Void)? = nil) {
        mockConnection.getData(self.urlPath,{data in
            guard let data = data else {
                failureCompletion?(NSError(domain: "no data", code: 123, userInfo: nil))
                return
            }
            var repositries: [RepositryModel]? = []
            for (key,value) in data {
                let dic = [key:value]
                let repositry = RepositryModel(dictionary: dic)
                repositries?.append(repositry)
            }
            successCompletion?(repositries)
        }, {error in
            failureCompletion?(error)
        })
    }
}
