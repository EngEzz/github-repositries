//
//  MockConnection.swift
//  roboustaTaskTests
//
//  Created by ahmed ezz on 10/5/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class MockConnection: Connection {
    private let mockTask: MockTask
    func getMockTask()->MockTask{return mockTask}
    private var isSuccess = true
    func dataSuccess(isSuccess:Bool)
    {
        self.isSuccess = isSuccess
    }
    init(data: [AnyHashable:Any]?, error: Error?,isSuccess:Bool = true)
    {
        self.isSuccess = isSuccess
        mockTask = MockTask(data: data, error:error)
    }
    override func getData(_ urlPath: String, _ successCompletion: (([AnyHashable : Any]?) -> Void)?, _ failureCompletion: ((Error?) -> Void)? = nil) {
        if isSuccess{ mockTask.successCompletionHandler = successCompletion }
        else{ mockTask.failureCompletionHandler = failureCompletion }
    }

}
