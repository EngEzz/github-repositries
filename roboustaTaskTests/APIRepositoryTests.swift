//
//  APIRepositoryTests.swift
//  roboustaTaskTests
//
//  Created by ahmed ezz on 10/5/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import XCTest
@testable import roboustaTask
class APIRepositoryTests: XCTestCase {
    private var dataRepositry:DataRepositry?
    private var connection:Connection?
    private var homeUseCase:HomeUseCase?
    override func setUp() {
        dataRepositry = DataRepositry()
        connection = Connection()
    }
    
    override func tearDown() {
        dataRepositry = nil
        connection = nil
    }
    func testGetReposDataWithExpectedURLHostAndPath()
    {
        XCTAssertEqual(dataRepositry?.urlPath, "repositories")
        XCTAssertEqual(connection?.urlHost, "https://api.github.com/")
    }
    func testGetReposSuccess()
    {
        let reposExpectation = expectation(description: "repositries")
        var reposData:[RepositryModel]?
        dataRepositry?.getUserRepos({data in
            reposData = data
            reposExpectation.fulfill()
        }, {error in
            
        })
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(reposData)
        }
    }
    func testGetReposSuccessReturnRepos()
    {
        let jsonData:[AnyHashable:Any] = ["name": "gritt","description": "**Grit is no longer maintained. Check out libgit2/rugged.** Grit gives you object oriented read/write access to Git repositories via Ruby."]
        let mockConnection = MockConnection(data: jsonData, error: nil)
        let mockRepositry = MockRepositry(mockConnection: mockConnection)
        let reposExpectation = expectation(description: "repositries")
        var reposData:[RepositryModel]?
        mockRepositry.getUserRepos({data in
            reposData = data ?? [] 
            reposExpectation.fulfill()
        }, {error in
            
        })
        mockConnection.getMockTask().loadSuccess()
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(reposData)
        }
    }
    func testGetRepsWhenResponseErrorReturnsError()
    {
        let error = NSError(domain: "error", code: 1, userInfo: nil)
        let mockConnection = MockConnection(data: nil, error: error,isSuccess:false)
        let mockRepositry = MockRepositry(mockConnection: mockConnection)
        let errorExpectation = expectation(description: "error")
        var errorResponse:Error?
        mockRepositry.getUserRepos(nil, {error in
            errorResponse = error 
            errorExpectation.fulfill()
        })
        mockConnection.getMockTask().loadError()
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(errorResponse)
        }
    }
    func testGetRepsWhenWhenEmptyDataReturnsError()
    {
        let mockConnection = MockConnection(data: nil, error: nil)
        let mockRepositry = MockRepositry(mockConnection: mockConnection)
        let errorExpectation = expectation(description: "error")
        var errorResponse:Error?
        mockRepositry.getUserRepos(nil
         ,{error in
            errorResponse = error
            errorExpectation.fulfill()
        })
        mockConnection.getMockTask().loadSuccess()
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(errorResponse)
        }
    }
}
