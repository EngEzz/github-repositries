//
//  MockTask.swift
//  roboustaTaskTests
//
//  Created by ahmed ezz on 10/5/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class MockTask {
    private let data: [AnyHashable:Any]?
    private let error: Error?
    var successCompletionHandler: (([AnyHashable:Any]?) -> Void)?
    var failureCompletionHandler: ((Error?) -> Void)?
    init(data: [AnyHashable:Any]?, error: Error?)
    {
        self.data = data
        self.error = error
    }
    func loadSuccess() {
        DispatchQueue.main.async {
            self.successCompletionHandler?(self.data)
        }
    }
    func loadError() {
        DispatchQueue.main.async {
            self.failureCompletionHandler?(self.error)
        }
    }
}
