//
//  HomeVC.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
import RxSwift
class HomeVC: UIViewController,HomeVCDelegate {
    var bag: DisposeBag = DisposeBag()
    var homeVM: HomeViewModel?
    @IBOutlet weak var repositriesDataCV: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeViews()
    }
    private func initializeViews()
    {
        initCollection()
        let _ = homeVM?.loadData()?.subscribe(onNext:{ [weak self] _ in
            self?.repositriesDataCV.reloadData()
            }, onError:{[weak self] error in
                guard let _ = self else {return}
                print("Error:\(error)")
            }).disposed(by: bag)
    }
    private func initCollection()
    {
        repositriesDataCV.registerCell(cellName: "\(RepositryCVC.self)")
        repositriesDataCV.dataSource = self
        repositriesDataCV.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Github Repositries"
    }
}
protocol HomeVCDelegate {
    var homeVM:HomeViewModel? { get set }
    var bag:DisposeBag { get set}
}
