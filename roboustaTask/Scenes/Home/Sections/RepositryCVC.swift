//
//  RepositryCVC.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class RepositryCVC: UICollectionViewCell {

    @IBOutlet private weak var ownerNameLb: UILabel!
    @IBOutlet private weak var repoNameLb: UILabel!
    @IBOutlet private weak var ownerIV: UIImageView!
    
    public func configureCell(repoViewViewModel:RepositryViewViewModel?)
    {
        ownerNameLb.text = repoViewViewModel?.owner?.ownerName ?? ""
        repoNameLb.text = repoViewViewModel?.repoName ?? ""
        ownerIV.loadImageFromUrl(imgUrl: repoViewViewModel?.owner?.ownerImage ?? "" )
    }
    override func awakeFromNib()
    {
        super.awakeFromNib()
        ownerIV.imageAsCircle()
        contentView.setBorder()
    }
    
}
