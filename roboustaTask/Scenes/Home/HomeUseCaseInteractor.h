//
//  HomeUseCaseInteractor.h
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#ifndef HomeUseCaseInteractor_h
#define HomeUseCaseInteractor_h

@protocol HomeUseCaseInteractorOutput<NSObject>
@required
-(void) fetchRepositriesSuccess:(NSArray<RepositryModel *>*) repositriesData;
-(void) fetchRepositriesWithError:(NSError*) error;
@end

@protocol HomeUseCaseInteractorInput<NSObject>
@required
@property(nonatomic)DataRepositry* dataRepositry;
@property(nonatomic)id<HomeUseCaseInteractorOutput> useCaseInteractor;
-(void)fetchRepositries;
@end
#endif /* HomeUseCaseInteractor_h */
