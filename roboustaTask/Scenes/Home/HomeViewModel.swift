//
//  HomeVM.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
import RxSwift
class HomeViewModel:NSObject,HomeViewModelDelegate
{
    var router: HomeRouterProtocol?
    var useCase: HomeUseCaseInteractorInput?
    var repositriesData: [RepositryModel]?
    var loadObserver: PublishSubject<Void>? = PublishSubject<Void>()
    init(useCase:HomeUseCaseInteractorInput,router:HomeRouterProtocol) {
        self.useCase = useCase
        self.router = router
    }
    func loadData()->Observable<Void>?
    {
        useCase?.fetchRepositries()
        return loadObserver?.asObserver()
    }
    func numberOfItems() -> Int {
        return repositriesData?.count ?? 0
    }
    func viewModelForRepositry(at index: Int) -> RepositryViewViewModel? {
        return repositriesData?.count == 0 ? nil : RepositryViewViewModel(repositryModel: repositriesData?[index])
    }
    func selectRepositry(at index: Int) {
        router?.pushToRepoDetails(with: repositriesData?[index])
    }
}
extension HomeViewModel:HomeUseCaseInteractorOutput
{
    func fetchRepositriesSuccess(_ repositriesData: [RepositryModel])
    {
        self.repositriesData = repositriesData
        loadObserver?.onNext(Void())
    }
    func fetchRepositriesWithError(_ error: Error)
    {
        loadObserver?.onError(error)
    }
}
protocol HomeViewModelDelegate {
    var repositriesData:[RepositryModel]? { get set }
    var loadObserver:PublishSubject<Void>? { get set }
    var router:HomeRouterProtocol?{get set}
    var useCase:HomeUseCaseInteractorInput?{get set}
    func loadData()->Observable<Void>?
    func numberOfItems()->Int
    func viewModelForRepositry(at index:Int)->RepositryViewViewModel?
    func selectRepositry(at index:Int)
}
