//
//  HomeUseCase.m
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import "HomeUseCase.h"

@implementation HomeUseCase 
@synthesize dataRepositry;
@synthesize useCaseInteractor;
- (instancetype)init
{
    self = [super init];
    self.dataRepositry = [[DataRepositry alloc]init];
    return self;
}
- (void)fetchRepositries
{
    [self.dataRepositry getUserRepos:^(NSArray<RepositryModel*>* repositires){
        [self.useCaseInteractor fetchRepositriesSuccess:repositires];
    }:^(NSError* error) {
        [self.useCaseInteractor fetchRepositriesWithError:error];
    }];
}
@end
