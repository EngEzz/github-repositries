//
//  HomeRouter.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class HomeRouter:HomeRouterProtocol
{
    weak var vc: UIViewController?
    init(viewController:UIViewController) {
        self.vc = viewController
    }
    static func createHome() -> UIViewController {
        let homeVC = HomeVC.loadFromNib()
        let homeRouter = HomeRouter(viewController: homeVC)
        let homeUseCase = HomeUseCase()
        let homeVM = HomeViewModel(useCase: homeUseCase, router: homeRouter)
        homeUseCase.useCaseInteractor = homeVM
        homeVC.homeVM = homeVM
        return homeVC
    }
    func pushToRepoDetails(with repositry: RepositryModel?) {
        let repoDetailsVC = RepositryDetailsRouter.createRepositryDetails(with: repositry)
        self.vc?.navigationController?.pushViewController(repoDetailsVC, animated: true)
    }
}
protocol HomeRouterProtocol:AnyObject {
    var vc:UIViewController?{get set}
    static func createHome()->UIViewController
    func pushToRepoDetails(with repositry:RepositryModel?)
}
