
//
//  RepositryViewModel.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

struct RepositryViewViewModel {
    private var repositryModel:RepositryModel?
    init(repositryModel:RepositryModel?)
    {
        self.repositryModel = repositryModel
    }
    var repoName:String?{return repositryModel?.repoName ?? "" }
    var owner:Owner?{return repositryModel?.owner }
    var ownerImageLink:String?{return repositryModel?.owner?.ownerImage ?? "" }
}
