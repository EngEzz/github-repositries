//
//  HomeUseCase.h
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataRepositry.h"
#import "HomeUseCaseInteractor.h"
NS_ASSUME_NONNULL_BEGIN
@interface HomeUseCase : NSObject<HomeUseCaseInteractorInput>

@end



NS_ASSUME_NONNULL_END
