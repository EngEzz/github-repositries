//
//  RepositryDetailsVC.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
import RxSwift
class RepositryDetailsVC: UIViewController,RepositryDetailsVCDelegate {
    @IBOutlet private weak var ownerIV: UIImageView!
    @IBOutlet private weak var repoNameLb: UILabel!
    @IBOutlet private weak var ownerNameLb: UILabel!
    @IBOutlet private weak var descriptionLb: UILabel!
    var bag: DisposeBag = DisposeBag()
    var repoDetailsVM: RepositryDetailsViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeViews()
    }
    private func initializeViews()
    {
        let _ = repoDetailsVM?.loadData()?.subscribe(onNext: {[weak self] _ in
            self?.loadViewData()
            }).disposed(by: bag)
    }
    func loadViewData() {
        ownerIV.loadImageFromUrl(imgUrl: repoDetailsVM?.repositry?.owner?.ownerImage ?? "" )
        repoNameLb.text = repoDetailsVM?.repositry?.repoName ?? ""
        ownerNameLb.text = repoDetailsVM?.repositry?.owner?.ownerName ?? ""
        descriptionLb.text = repoDetailsVM?.repositry?.repoDescription ?? "" 
    }
}
protocol RepositryDetailsVCDelegate {
    var repoDetailsVM:RepositryDetailsViewModel?{get set}
    var bag:DisposeBag{get set}
    func loadViewData()
}
