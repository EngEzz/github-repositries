//
//  RepositryDetailsRouter.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class RepositryDetailsRouter:RepositryDetailsRouterProtocol {
    weak var vc: UIViewController?
    init(viewController:UIViewController) {
        self.vc = viewController
    }
    static func createRepositryDetails(with repositry: RepositryModel?) -> UIViewController {
        let repoDetailsVC = RepositryDetailsVC.loadFromNib()
        let router = RepositryDetailsRouter(viewController: repoDetailsVC)
        let useCase = RepositryDetailsUseCase()
        let repoDetailsVM = RepositryDetailsViewModel(useCase: useCase, router: router)
        repoDetailsVC.repoDetailsVM = repoDetailsVM
        useCase.useCase = repoDetailsVM
        useCase.repositry = repositry
        return repoDetailsVC
    }    
}
protocol RepositryDetailsRouterProtocol:AnyObject {
    var vc:UIViewController? { get set }
    static func createRepositryDetails(with repositry:RepositryModel?)->UIViewController
}
