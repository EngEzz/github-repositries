//
//  RepositryDetailsUseCase.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class RepositryDetailsUseCase:RepositryDetailsUseCaseInteractorInput {
    var useCase: RepositryDetailsUseCaseInteractorOutput?
    var repositry: RepositryModel?
    func getRepoData() {
        if let _ = repositry {
            self.useCase?.fetchRepo(repositry: repositry)
        }
    }
}
protocol RepositryDetailsUseCaseInteractorInput {
    var repositry:RepositryModel?{get set}
    var useCase:RepositryDetailsUseCaseInteractorOutput?{get set}
    func getRepoData()
}
protocol RepositryDetailsUseCaseInteractorOutput {
    func fetchRepo(repositry:RepositryModel?)
}
