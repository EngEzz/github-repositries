//
//  RepositryDetailsViewModel.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
import RxSwift
class RepositryDetailsViewModel: RepositryDetailsViewModelDelegate {
    var repositry: RepositryModel?
    var loadObserver: ReplaySubject<Void>? = ReplaySubject<Void>.create(bufferSize: 2)
    var router: RepositryDetailsRouterProtocol?
    var useCase: RepositryDetailsUseCaseInteractorInput?
    init(useCase:RepositryDetailsUseCaseInteractorInput?,router:RepositryDetailsRouterProtocol?) {
        self.useCase = useCase
        self.router = router
    }
    func loadData() -> Observable<Void>? {
        self.useCase?.getRepoData()
        return loadObserver?.asObserver()
    }
}
extension RepositryDetailsViewModel:RepositryDetailsUseCaseInteractorOutput{
    func fetchRepo(repositry: RepositryModel?) {
        self.repositry = repositry
        self.loadObserver?.onNext(Void())
    }
}
protocol RepositryDetailsViewModelDelegate {
    var repositry:RepositryModel?{get set}
    var loadObserver:ReplaySubject<Void>? { get set }
    var router:RepositryDetailsRouterProtocol?{get set}
    var useCase:RepositryDetailsUseCaseInteractorInput?{get set}
    func loadData() -> Observable<Void>?
}
