//
//  RepositryModel.m
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import "RepositryModel.h"

@implementation RepositryModel
- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.owner = [[Owner alloc]initWithDictionary:dictionary[@"owner"]];
        self.repoName = dictionary[@"name"];
        self.repoDescription = dictionary[@"description"];
    }
    return self;
}
@end
