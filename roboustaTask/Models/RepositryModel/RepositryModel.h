//
//  RepositryModel.h
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Owner.h"
NS_ASSUME_NONNULL_BEGIN

@interface RepositryModel : NSObject
@property(nonatomic,nullable)Owner* owner;
@property(nonatomic,nullable)NSString* repoName;
@property(nonatomic,nullable)NSString* repoDescription;
- (id)initWithDictionary:(NSDictionary*)dictionary;
@end

NS_ASSUME_NONNULL_END
