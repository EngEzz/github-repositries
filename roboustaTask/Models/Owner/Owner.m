//
//  Owner.m
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import "Owner.h"

@implementation Owner

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.ownerImage = dictionary [@"avatar_url"];
        self.ownerName = dictionary [@"login"];
    }
    return self;
}
@end
