//
//  Owner.h
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Owner : NSObject
@property(nonatomic,nullable)NSString* ownerName;
@property(nonatomic,nullable)NSString* ownerImage;
- (id)initWithDictionary:(NSDictionary*)dictionary;
@end

NS_ASSUME_NONNULL_END
