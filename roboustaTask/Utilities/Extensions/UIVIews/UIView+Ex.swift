//
//  UIView+Ex.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    func setBorder(borderColor:UIColor = UIColor.lightGray,cornerRadious:Int = 15)
    {
        self.clipsToBounds = true
        self.layer.borderColor = borderColor.withAlphaComponent(0.4).cgColor
        self.layer.borderWidth = 0.6
        self.layer.cornerRadius = CGFloat(cornerRadious)
    }
}
