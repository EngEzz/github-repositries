//
//  UIImageView+Ex.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import Foundation
import UIKit
import SDWebImageWebPCoder
extension UIImageView {
    public func imageAsCircle()
    {
        self.layer.cornerRadius = self.bounds.width / 2
        self.clipsToBounds = true
    }
    func loadImageFromUrl (imgUrl : String?,defaultImage:String = "Github-logo"){
        let defaultImage = UIImage(named: defaultImage)
        if imgUrl == "" || imgUrl == nil
        {
            self.image = defaultImage
            return
            
        }
        let webPCoder = SDImageWebPCoder.shared
        SDImageCodersManager.shared.addCoder(webPCoder)
        guard let url = URL(string: imgUrl ?? "" ) else {
            self.image = defaultImage
            return
        }
        DispatchQueue.main.async {
            self.sd_setImage(with: url, placeholderImage: defaultImage, options: .highPriority, progress: nil, completed: nil)
        }
    }
}
