//
//  UICollectionView+Ex.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/4/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import Foundation
import UIKit
extension UICollectionView{
    func registerCell(cellName:String)
    {
        self.register(UINib(nibName: cellName, bundle: nil), forCellWithReuseIdentifier: cellName)
    }
    func getCellWithIdentifier<T:UICollectionViewCell>(cell:T.Type,identifier:String,indexPath:IndexPath)->T
    {
        return self.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? T ?? T()
    }
}
