//
//  AppDelegate+Ex.swift
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

extension AppDelegate {
    
    internal func didFinishLaunching()
    {
        let mainVC = HomeRouter.createHome()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UINavigationController(rootViewController: mainVC)
        self.window?.makeKeyAndVisible()
    }
}
