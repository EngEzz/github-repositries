//
//  DataRepositry.m
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import "DataRepositry.h"
#import "Connection.h"

@implementation DataRepositry

- (instancetype)init
{
    self = [super init];
    self.urlPath = @"repositories";
    return self;
}
- (void)getUserRepos:(void (^)(NSArray<RepositryModel *> * _Nullable))successCompletion :(void (^)(NSError * _Nullable))failureCompletion
{
    Connection* connection = [Connection shared];
    [connection getData:self.urlPath: ^(NSDictionary* data) {
        NSMutableArray<RepositryModel*>* repositries = [[NSMutableArray alloc]init];
        if (data == nil)
        {
            NSError* error = [NSError errorWithDomain:@"no data" code:400 userInfo:nil];
            failureCompletion(error);
        }
        else
        {
            for(NSDictionary *dic in data)
            {
                RepositryModel *repositry = [[RepositryModel alloc] initWithDictionary:dic];
                [repositries addObject:repositry];
            }
            successCompletion(repositries);
        }
    }:^(NSError* error){
        failureCompletion(error);
    }];
}
@end
