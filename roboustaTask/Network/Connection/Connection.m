//
//  Connection.m
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import "Connection.h"
#import "RepositryModel.h"
@import AFNetworking;
@implementation Connection


static Connection *shared ;
+ (Connection *)shared{
    return shared = shared == nil ? [[Connection alloc] init] : shared;
}
- (instancetype)init
{
    self = [super init];
    self.urlHost = @"https://api.github.com/";
    return self;
}
- (void)getData:(NSString *)urlPath :(void (^)(NSDictionary * _Nullable))successCompletion :(void (^)(NSError * _Nullable))failureCompletion
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSString *fullURL = [NSString stringWithFormat:@"%@%@", self.urlHost, urlPath];
    NSURL *URL = [NSURL URLWithString:fullURL];
    if (URL == nil){failureCompletion([NSError errorWithDomain:@"server error with invalidUrl" code:500 userInfo:nil]);}
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request uploadProgress:nil
        downloadProgress:nil
        completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            failureCompletion(error);
        } else {
            NSLog(@"Response Data : %@", responseObject);
            NSDictionary* dataDic = responseObject;
            successCompletion(dataDic);
        }
    }];
    [dataTask resume];
}
@end
