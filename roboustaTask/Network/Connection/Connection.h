//
//  Connection.h
//  roboustaTask
//
//  Created by ahmed ezz on 10/3/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositryModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface Connection : NSObject
+(Connection*) shared;
@property(nonatomic)NSString* urlHost;
-(void) getData: (NSString*) urlPath : (nullable void (^)( NSDictionary* _Nullable dataDic))successCompletion :(nullable void (^)( NSError* _Nullable error))failureCompletion;

@end

NS_ASSUME_NONNULL_END
